import java.util.ArrayList;
import java.util.Scanner;


public class Add2 {
	private static ArrayList<Subject> arrSubject = new ArrayList<Subject>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		String subject="",topic;
		while(!subject.equals("end")){
			System.out.print("Enter subject : ");
			subject = scan.nextLine();
			if(subject.equals("end")){
				break;
			}
			System.out.print("Enter topic : ");
			topic = scan.nextLine();
			arrSubject.add(new Subject(subject,topic));
			sortArrayList(arrSubject);
		}
		System.out.print("Enter subject to view : ");
		String viewSubject = scan.nextLine();
		for(int i=0;i<arrSubject.size();i++){
			if(arrSubject.get(i).getSubject().equals(viewSubject)){
				System.out.print(arrSubject.get(i).getTopic() + " ");
			}
		}
		
		
	}
	public static void sortArrayList(ArrayList<Subject> arrSubject){
		ArrayList<Subject> subjectTemp = new ArrayList<Subject>();
		for(int i=0;i<arrSubject.size();i++){
			for(int j=0;j<arrSubject.size();j++){
				if(arrSubject.get(i).getSubject().compareTo(arrSubject.get(j).getSubject())>0){
					Subject temp = arrSubject.get(i);
					arrSubject.set(i, arrSubject.get(j));
					arrSubject.set(j, temp);
				}
			}
		}
	}

}
