
public class Subject {
	private String subject,topic;
	public Subject(String subject,String topic){
		this.subject = subject;
		this.topic = topic;
	}
	public String getSubject(){
		return subject;
	}
	public String getTopic(){
		return topic;
	}
}
