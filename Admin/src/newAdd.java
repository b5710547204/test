import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class newAdd {
	private static ArrayList<String> arrSubject = new ArrayList<String>();

	private static ArrayList<List<String>> arrTopic = new ArrayList<List<String>>();
	private static List<String> listTopic = new ArrayList<String>();
	private static String[] finalTopic1;
	private static ArrayList<String> finalTopic2 = new ArrayList<String>();

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.println("Add");
		arrSubject.add("Select Subject");
		
		String subject = "";
		String topic = "";
		int checkSubject = 0;
		int beforeCheckSubject = 0;
		
		//-----------------start memory code------------------//
		do{

			System.out.print("Subject : ");
			subject = scanner.nextLine();
//			checkSubject = 0;
			if(subject.equals("end")){
				if((checkSubject==0&&beforeCheckSubject==0)||(checkSubject==1&&beforeCheckSubject==1)){
					finalTopic1 = new String[finalTopic2.size()];
					for(int i = 0 ; i<finalTopic2.size() ; i++){
						finalTopic1[i] = finalTopic2.get(i);
					}
					listTopic = Arrays.asList(finalTopic1);
					arrTopic.add(listTopic);
					finalTopic2.clear();
				}
				break;
			}
			
			checkSubject = 0;
			
			for(int i = 0 ; i<arrSubject.size() ; i++){
				if(subject.equals(arrSubject.get(i))){
					checkSubject = 1;
				}		

			}		
			
			if(checkSubject==0)
				arrSubject.add(subject);

			System.out.print("Topic : ");
			topic = scanner.nextLine();

			if(checkSubject==1&&beforeCheckSubject==0){
				finalTopic2.add(topic);
				beforeCheckSubject = 1;

			}
			
			else if(checkSubject==0&&beforeCheckSubject==0){
				finalTopic2.add(topic);
				beforeCheckSubject = 0;
			}
			
			else if(checkSubject==1&&beforeCheckSubject==1){
				finalTopic2.add(topic);
				beforeCheckSubject = 1;
			}
			
			else if(checkSubject==0&&beforeCheckSubject==1){
				finalTopic1 = new String[finalTopic2.size()];
				for(int i = 0 ; i<finalTopic2.size() ; i++){
					finalTopic1[i] = finalTopic2.get(i);
				}
				listTopic = Arrays.asList(finalTopic1);
				arrTopic.add(listTopic);
				finalTopic2.clear();
				finalTopic2.add(topic);
				beforeCheckSubject = 1;
			}

		}while(!subject.equals("end"));
		
		//-------------------------------End of memory code--------------------//

		int subjectConnect = 0;

		System.out.print("Subject => ");
		for(int i = 1 ; i<arrSubject.size() ; i++){
			System.out.print(arrSubject.get(i)+" ");
		}
		System.out.println();
		System.out.print("Select your subject : ");
		String subjectChoose = scanner.nextLine();
		for(int i = 0 ; i<arrSubject.size() ; i++){
			if(subjectChoose.equals(arrSubject.get(i)))
				subjectConnect = i-1;
		}

		System.out.print("Topic => ");
		for(int i = 0 ; i<arrTopic.get(subjectConnect).size() ; i++){
			System.out.print(arrTopic.get(subjectConnect).get(i)+" ");
		}
	}

}
