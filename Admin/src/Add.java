import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Add {
	protected static ArrayList<String> arrayList1 = new ArrayList<String>();
	protected static ArrayList<List<String>> arrayList2 = new ArrayList<List<String>>();
	protected static List<String> topic = new ArrayList<String>();

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		//		arrayList1.add("Physic");
		//		arrayList1.add("Math");
		String[] subject = new String[]{"Physic","Math"};
		for(int i = 0 ; i<subject.length ; i++){
			arrayList1.add(subject[i]);
		}

		String[] topicPhy = new String[]{"Wave","Light"};
		//		topic.add(topicPhy);
		//		arrayList2.add(topic);
		//		for(int i = 0 ; i<topicPhy.length ; i++){
		topic = Arrays.asList(topicPhy);
		arrayList2.add(topic);
		//		}

		String[] topicMath = new String[]{"Cal 1","Cal 2"};
		//		topic.add(topicMath);
		//		arrayList2.add(topic);
		//		for(int i = 0 ; i<topicPhy.length ; i++){
		topic = Arrays.asList(topicMath);
		arrayList2.add(topic);
		//		}

		System.out.print("Subject : ");
		for(int i =0 ; i<arrayList1.size() ; i++){
			System.out.print(arrayList1.get(i)+" ");
		}
		System.out.println();
		System.out.print("Topic : ");
		for(int i = 0 ; i<arrayList2.size(); i++){
			for(int j = 0 ; j<arrayList2.size();j++){
				System.out.print(arrayList2.get(i).get(j));
				if(i==arrayList2.size()-1&&j==arrayList2.size()-1)
					break;
				System.out.print(" ,");
			}
		}
		System.out.println();
		System.out.print("Which subject do you want? : ");
		String subjectText = scanner.nextLine();
		int subject2 = 0;
		int check = 0;
		for(int i = 0 ; i<arrayList1.size() ; i++){
			if(subjectText.equals(arrayList1.get(i))){
				subject2 = i;
				check = 1;
			}
		}
		if(check==1){
			System.out.print("You can select from this => ");
			for(int i = 0 ; i<arrayList2.size() ; i++){
				System.out.print(arrayList2.get(subject2).get(i)+" ");
			}
		}
		else
			System.out.println("Error!!");
	}

}
